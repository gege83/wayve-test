import FilterPanel from './FilterPanel';
import {fireEvent, render} from "@testing-library/react";
import React from "react";

describe('FilterPanel', () => {
  it('should trigger onFilterChange when action checkbox changed', async () => {
    const filter = {selectedActions: [], selectedReasons: [], range: {min: 0, max: 3}};
    const onFilterChange = jest.fn();
    const {getByLabelText} = render(<FilterPanel onFilterChange={onFilterChange} filter={filter} max={3}/>);
    const stopCheckbox = await getByLabelText(/stop/i);
    fireEvent.click(stopCheckbox);
    await expect(onFilterChange).toHaveBeenCalledWith(expect.objectContaining({selectedActions: ["Stop"]}));
  });
  it('should trigger onFilterChange when reason checkbox changed', async () => {
    const filter = {selectedActions: [], selectedReasons: [], range: {min: 0, max: 3}};
    const onFilterChange = jest.fn();
    const {getByLabelText} = render(<FilterPanel onFilterChange={onFilterChange} filter={filter} max={3}/>);
    const stopCheckbox = await getByLabelText(/signal/i);
    fireEvent.click(stopCheckbox);
    await expect(onFilterChange).toHaveBeenCalledWith(expect.objectContaining({selectedReasons: ["Signal"]}));
  });
  it('should show Range', async () => {
    const filter = {selectedActions: [], selectedReasons: [], range: {min: 1, max: 2}};
    const {getByText} = render(<FilterPanel onFilterChange={jest.fn()} filter={filter} max={3}/>);
    expect(await getByText(/range/i)).toBeInTheDocument();
    expect(await getByText(/0/i)).toBeInTheDocument();
    expect(await getByText(/1/i)).toBeInTheDocument();
    expect(await getByText(/2/i)).toBeInTheDocument();
    expect(await getByText(/3/i)).toBeInTheDocument();
  });
});
