import ReasonFilter from './ReasonFilter';
import {fireEvent, render} from "@testing-library/react";
import ActionFilter from "./ActionFilter";
import React from "react";

describe('ActionFilter', () => {
  it('should show action filters', async () => {
    const {getByText} = render(<ReasonFilter selectedReasons={[]} onChange={() => {
    }}/>);
    expect(await getByText("Filter by reasons")).toBeInTheDocument();
  });

  test.each`
    reason
    ${"Vehicle"}
    ${"Signal"}
    ${"Cyclist"}
    ${"Pedestrian"}
    ${"ParkedVehicle"}
    ${"Roadworks"}
    ${"OtherStatic"}
    ${"OtherDynamic"}
    `('should select reason $reason', ({reason}) => {
    const onChange = jest.fn();
    const {getByLabelText} = render(<ReasonFilter selectedReasons={[]} onChange={onChange}/>);
    const checkbox = getByLabelText(reason);
    fireEvent.click(checkbox);
    expect(onChange).toHaveBeenCalledWith(expect.objectContaining({selectedReasons: [reason]}))
  });

  it('should remove from the selected if it was on the list', () => {
    const onChange = jest.fn();
    const action = "Vehicle";
    const {getByLabelText} = render(<ReasonFilter selectedReasons={["Vehicle", "Cyclist"]} onChange={onChange}/>);
    const checkbox = getByLabelText(action);
    fireEvent.click(checkbox);
    expect(onChange).toHaveBeenCalledWith(expect.objectContaining({selectedReasons: ["Cyclist"]}))
  });

});
