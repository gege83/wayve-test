import {fireEvent, render} from '@testing-library/react';
import React from "react";
import ActionFilter from './ActionFilter';

describe('ActionFilter', () => {
  it('should show action filters', async () => {
    const {getByText} = render(<ActionFilter selectedActions={[]} onChange={() => {
    }}/>);
    expect(await getByText("Filter by actions")).toBeInTheDocument();
  });

  test.each`
    action
    ${"Stop"}
    ${"GiveWay"}
    ${"Follow"}
    ${"Overtake"}
    ${"Undertake"}
    ${"Join"}
    ${"Negotiation"}
    ${"Park"}
    ${"EmergencyManeuver"}
    `('should select action $action', ({action}) => {
    const onChange = jest.fn();
    const {getByLabelText} = render(<ActionFilter selectedActions={[]} onChange={onChange}/>);
    const checkbox = getByLabelText(action);
    fireEvent.click(checkbox);
    expect(onChange).toHaveBeenCalledWith(expect.objectContaining({selectedActions: [action]}))
  });

  it('should remove from the selected if it was on the list', () => {
    const onChange = jest.fn();
    const action = "Stop";
    const {getByLabelText} = render(<ActionFilter selectedActions={["Stop", "GiveWay"]} onChange={onChange}/>);
    const checkbox = getByLabelText(action);
    fireEvent.click(checkbox);
    expect(onChange).toHaveBeenCalledWith(expect.objectContaining({selectedActions: ["GiveWay"]}))
  });

});
