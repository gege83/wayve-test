import React, {PropsWithChildren, ReactElement} from "react";
import {Reason} from "../data/data";
import CheckboxFilter from "./CheckboxFilter";

type OnChange = (state: { selectedReasons: Reason[] }) => void;

interface FilterProps {
  onChange: OnChange;
  selectedReasons: Reason[]
}

export const possibleReasons: Reason[] = ["Vehicle",
  "Signal",
  "Cyclist",
  "Pedestrian",
  "ParkedVehicle",
  "Roadworks",
  "OtherStatic",
  "OtherDynamic"];

export default function ReasonFilter(props: PropsWithChildren<FilterProps>): ReactElement {
  const onChange = (selectedItems: string[]) => {
    props.onChange({selectedReasons: selectedItems as Reason[]})
  };
  return <>
    <h1>Filter by reasons</h1>
    <CheckboxFilter
      onChange={onChange}
      selectedItems={props.selectedReasons}
      items={possibleReasons}/>
  </>;
}
