import ActionFilter from "./ActionFilter";
import ReasonFilter from "./ReasonFilter";
import InputRange, {Range} from "react-input-range";
import React from "react";
import {Action, Reason} from "../data/data";

export interface FilterOptions {
  range: Range;
  selectedActions: Action[];
  selectedReasons: Reason[];
}

export default function FilterPanel({onFilterChange, filter, max}: { onFilterChange: (filter: FilterOptions) => void, filter: FilterOptions, max: number }) {
  return <>
    <div style={{display: "flex", justifyContent: "space-around"}}>
      <div>
        <ActionFilter
          onChange={(state) => {
            onFilterChange({...filter, ...state});
          }}
          selectedActions={filter.selectedActions}/>
      </div>
      <div>
        <ReasonFilter
          onChange={(state) => {
            onFilterChange({...filter, ...state});
          }}
          selectedReasons={filter.selectedReasons}/>
      </div>
    </div>
    <div style={{width: '80%', margin: 'auto auto'}}>
      <h1>Select range of </h1>
      <InputRange
        maxValue={max}
        minValue={0}
        onChange={(range) => {
          onFilterChange({...filter, range: range as Range});
        }}
        value={filter.range}/>

    </div>
  </>
}
