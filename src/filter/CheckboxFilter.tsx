import React, {ReactElement} from "react";

type OnChange = (selectedItems: string[]) => void;

interface FilterProps {
  onChange: OnChange;
  selectedItems: string[]
  items: string[]
}

function selectedItemHandler(selectedItems: string[], item: string, onChange: OnChange) {
  return ({target}: React.ChangeEvent<HTMLInputElement>) => {
    let finalSelection: string[] = [...selectedItems, item];
    if (!target.checked) {
      finalSelection = finalSelection.filter(element => element !== item)
    }
    onChange(finalSelection);
  };
}

function renderFilter({selectedItems, onChange, items}: FilterProps) {
  return items.map((item) => {
    const selected = selectedItems.includes(item);
    return (
      <div key={item}>
        <input type="checkbox"
               name="action"
               checked={selected}
               id={item}
               onChange={selectedItemHandler(selectedItems, item, onChange)}/>
        <label htmlFor={item}>{item}</label>
      </div>);
  });
}

export default function CheckboxFilter(props: FilterProps): ReactElement {

  return <div style={{textAlign: "left"}}>
    {
      renderFilter(props)
    }
  </div>;
}
