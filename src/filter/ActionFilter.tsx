import React, {PropsWithChildren, ReactElement} from "react";
import {Action} from "../data/data";
import CheckboxFilter from "./CheckboxFilter";

type OnChange = (state: { selectedActions: Action[] }) => void;

interface FilterProps {
  onChange: OnChange;
  selectedActions: Action[]
}

export const possibleActions: Action[] = ["Stop",
  "GiveWay",
  "Follow",
  "Overtake",
  "Undertake",
  "Join",
  "Negotiation",
  "Park",
  "EmergencyManeuver"];

export default function ActionFilter(props: PropsWithChildren<FilterProps>): ReactElement {
  const onChange = (selectedItems: string[]) => {
    props.onChange({selectedActions: selectedItems as Action[]})
  };
  return <>
    <h1>Filter by actions</h1>
    <CheckboxFilter
      onChange={onChange}
      selectedItems={props.selectedActions}
      items={possibleActions}/>
  </>;
}
