import {fireEvent, render} from '@testing-library/react';
import React from "react";
import CheckboxFilter from './CheckboxFilter';

describe('Filter', () => {
  test.each`
    item
    ${"item1"}
    ${"item2"}
    `('should select item $item', ({item}) => {
    const onChange = jest.fn();
    const {getByLabelText} = render(<CheckboxFilter items={["item1", "item2"]}
                                                    selectedItems={[]}
                                                    onChange={onChange}/>);
    const checkbox = getByLabelText(item);
    fireEvent.click(checkbox);
    expect(onChange).toHaveBeenCalledWith(expect.objectContaining([item]))
  });

  it('should remove from the selected if it was on the list', () => {
    const onChange = jest.fn();
    const item = "item1";
    const {getByLabelText} = render(<CheckboxFilter items={["item1", "item2"]}
                                                    selectedItems={["item1", "item2"]}
                                                    onChange={onChange}/>);
    const checkbox = getByLabelText(item);
    fireEvent.click(checkbox);
    expect(onChange).toHaveBeenCalledWith(["item2"])
  });

});

