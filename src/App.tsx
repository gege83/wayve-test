import React, {useEffect, useState} from 'react';
import './App.css';
import MapContainer, {MarkerData} from "./map/MapContainer";
import eventDataService from "./data/eventDataService";
import mapDataConverter from "./map/mapDataConverter";
import {Action, EventData, Reason} from "./data/data";
import {Range} from "react-input-range";
import 'react-input-range/lib/css/index.css';
import FilterPanel, {FilterOptions} from "./filter/FilterPanel";
import {possibleActions} from "./filter/ActionFilter";
import {possibleReasons} from "./filter/ReasonFilter";

export function filterMarketData(markerData: MarkerData[], range: Range, selectedActions: Action[], selectedReasons: Reason[]): MarkerData[] {
  return markerData.filter((markerData, index) =>
    index >= range.min && index <= range.max
    && selectedActions.includes(markerData.action)
    && selectedReasons.includes(markerData.reason)
  );
}

const App: React.FC = () => {
  const [markerData, setMarkerData] = useState<MarkerData[]>([]);
  const [filter, setFilter] = useState<FilterOptions>({
    range: {min: 0, max: 1},
    selectedActions: possibleActions,
    selectedReasons: possibleReasons
  });
  useEffect(() => {
    eventDataService().then((data: EventData[]): void => {
      setMarkerData(mapDataConverter(data));
      setFilter({...filter, range: {min: 0, max: data.length}});
    })
    // eslint-disable-next-line
  }, []);
  const renderMarkers = filterMarketData(markerData, filter.range, filter.selectedActions, filter.selectedReasons);
  return (
    <div className="App">
      <FilterPanel filter={filter} onFilterChange={setFilter} max={markerData.length || 1}/>
      <div style={{
        width: "500px",
        height: "500px",
        position: "relative",
        margin: "2em auto"
      }}>
        <MapContainer
          fullPath={markerData}
          eventMarkers={renderMarkers}/>
      </div>
    </div>
  );
};

export default App;
