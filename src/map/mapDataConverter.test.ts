import mapDataConverter from './mapDataConverter';
import {EventData} from "../data/data";

function buildEventData(eventData: Partial<EventData>): EventData {
  return {
    lat: 213,
    long: 44,
    reason: "Vehicle",
    action: "EmergencyManeuver",
    ...eventData
  };
}

describe('mapDataConverter', () => {
  it('should convert property long to lng', () => {
    const eventData = [buildEventData({long: 123})];
    expect(mapDataConverter(eventData)).toEqual([expect.objectContaining({lng: 123})]);
  });

  it('should keep property lat', () => {
    const eventData = [buildEventData({lat: 124})];
    expect(mapDataConverter(eventData)).toEqual([expect.objectContaining({lat: 124})]);
  });

  it('should keep property action', () => {
    const eventData = [buildEventData({action: 'EmergencyManeuver'})];
    expect(mapDataConverter(eventData)).toEqual([expect.objectContaining({action: 'EmergencyManeuver'})]);
  });

  it('should keep property reason', () => {
    const eventData = [buildEventData({reason: 'Cyclist'})];
    expect(mapDataConverter(eventData)).toEqual([expect.objectContaining({reason: 'Cyclist'})]);
  });

});
