import {EventData} from "../data/data";
import {MarkerData} from "./MapContainer";

export default function mapDataConverter(eventData: EventData[]): MarkerData[] {
  return eventData.map(({long, lat, action, reason}) => ({lng: long, lat, action, reason}));
}
