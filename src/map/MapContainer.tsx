import {GoogleApiWrapper, Map, MapProps, Marker, Polyline} from 'google-maps-react';
import environment from "../environment";
import React, {PropsWithChildren, ReactElement} from "react";
import {Action, Reason} from "../data/data";

interface Coordinate {
  lat: number;
  lng: number;
}

export interface MarkerData extends Coordinate {
  reason: Reason;
  action: Action;
}

interface EventMapProps extends MapProps {
  eventMarkers: MarkerData[];
  fullPath: Coordinate[];
}

function map(props: PropsWithChildren<EventMapProps>): ReactElement {
  return (
    <Map google={props.google} initialCenter={{lat: 52.2012444, lng: 0.136282548}} zoom={14}>
      {props.eventMarkers.map((markerData, index) => <Marker key={index}
                                                             title={`${markerData.action} ${markerData.reason}`}
                                                             position={markerData}/>)}
      <Polyline path={props.fullPath}/>
    </Map>);
}

export default GoogleApiWrapper({
  apiKey: environment.MAPS_API_KEY,
})(map);
