import eventData, {EventData} from "./data";

export default async function getData(): Promise<EventData[]> {
  return eventData;
}
