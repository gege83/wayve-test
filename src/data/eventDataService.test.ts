jest.mock('./data', () => [{some: 'data'}]);
import getEventData from "./eventDataService";

describe('getEventData', () => {
  it("should return all test data from the data file", async () => {
    expect(await getEventData()).toEqual([{some: 'data'}]);
  });
});
