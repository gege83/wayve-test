import React from 'react';
import {render} from '@testing-library/react';
import App, {filterMarketData} from './App';
import {MarkerData} from "./map/MapContainer";
import {Action, Reason} from "./data/data";

test('maps loading', () => {
  const {getByText} = render(<App/>);
  expect(getByText(/loading/i)).toBeInTheDocument();
});

describe('filterMarketData', () => {
  it('should return empty if no market data', () => {
    const marketData: MarkerData[] = [];
    const actions: Action[] = ["Stop"];
    const reasons: Reason[] = ["Signal"];
    const range = {min: 3, max: 5};
    expect(filterMarketData(marketData, range, actions, reasons)).toEqual([]);
  });
  it('should return empty if no action or reason is selected', () => {
    const marketData: MarkerData[] = [{reason: "Signal", action: "Stop", lat: 1, lng: 3}];
    const actions: Action[] = [];
    const reasons: Reason[] = [];
    const range = {min: 0, max: 1};
    expect(filterMarketData(marketData, range, actions, reasons)).toEqual([]);
  });
  it('should return empty if selected actions are not in the data', () => {
    const marketData: MarkerData[] = [{reason: "Signal", action: "Stop", lat: 1, lng: 3}];
    const actions: Action[] = ["EmergencyManeuver"];
    const reasons: Reason[] = ["Signal"];
    const range = {min: 0, max: 1};
    expect(filterMarketData(marketData, range, actions, reasons)).toEqual([]);
  });
  it('should return empty if selected reasons are not in the data', () => {
    const marketData: MarkerData[] = [{reason: "Signal", action: "Stop", lat: 1, lng: 3}];
    const actions: Action[] = ["Stop"];
    const reasons: Reason[] = ["Cyclist"];
    const range = {min: 0, max: 1};
    expect(filterMarketData(marketData, range, actions, reasons)).toEqual([]);
  });

  it('should return empty if Range doesn\'t contains selected action or reason', () => {
    const marketData: MarkerData[] = [
      {reason: "Signal", action: "Stop", lat: 1, lng: 3},
      {reason: "Signal", action: "Stop", lat: 1, lng: 3},
      {reason: "Cyclist", action: "Stop", lat: 1, lng: 3}];
    const actions: Action[] = ["Stop"];
    const reasons: Reason[] = ["Cyclist"];
    const range = {min: 0, max: 1};
    expect(filterMarketData(marketData, range, actions, reasons)).toEqual([]);
  });
  it('should return data if data is in the range and selected action and reason', () => {
    const marketData: MarkerData[] = [
      {reason: "Signal", action: "Stop", lat: 1, lng: 3},
      {reason: "Cyclist", action: "Stop", lat: 1, lng: 3}
    ];
    const actions: Action[] = ["Stop"];
    const reasons: Reason[] = ["Cyclist"];
    const range = {min: 0, max: 2};
    expect(filterMarketData(marketData, range, actions, reasons)).toEqual([{
      reason: "Cyclist",
      action: "Stop",
      lat: 1,
      lng: 3
    }]);
  });
});
