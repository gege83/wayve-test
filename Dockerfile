FROM node:12.13.1-alpine3.9 as base

WORKDIR /wayve-test
COPY ./package.json .
COPY ./package-lock.json .
RUN npm ci

COPY . .
RUN npm run build
RUN npm run test:ci

FROM base as runtime
RUN npm start
EXPOSE 3000
