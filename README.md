Wayve data visualization project.

# Assumptions

- the data is in time order
- we are curious about events and reasons if they calculated properly. 
- we might have full GPS path which will be given as well. For now I used the given data as a module.
- the data will come from an API

# Dependencies

Docker 

# Environment variables

- REACT_APP_MAPS_API_KEY to show google maps

# running tests
`make build`
or 
`npm test`

# run the project:
`make start`
